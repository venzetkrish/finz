package com.example.hp.finz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class OnBoardActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private int[] layouts;
    private ImageButton btnSkip,btnNext;
    private OnBoardAdapter pagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);
        if(!isFirstTimeStartApp())
        {   startMainActivity();
            finish();
        }



        viewPager=(ViewPager)findViewById(R.id.viewPage);
        btnSkip=(ImageButton)findViewById(R.id.skipButton);
        btnNext=(ImageButton)findViewById(R.id.nextButton);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMainActivity();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage=viewPager.getCurrentItem()+1;
                if(currentPage<layouts.length){
                    viewPager.setCurrentItem(currentPage);
                }
                else{
                    startMainActivity();
                }
            }
        });
        layouts=new int[]{R.layout.screen_one,R.layout.screen_two,R.layout.screen_three};
        pagerAdapter=new OnBoardAdapter(layouts,getApplicationContext());
        viewPager.setAdapter(pagerAdapter);
    }
    private boolean isFirstTimeStartApp(){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("firsttime", Context.MODE_PRIVATE);
        return ref.getBoolean("FirstTimeStartFlag",true);
    }

    private void setFirstTimeStartStatus(boolean stt){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("firsttime", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=ref.edit();
        editor.putBoolean("FirstTimeStartFlag",stt);
        editor.commit();
    }

    private void startMainActivity(){
        setFirstTimeStartStatus(false);
        startActivity(new Intent(OnBoardActivity.this,FinzActivity.class));
        finish();
    }
}
