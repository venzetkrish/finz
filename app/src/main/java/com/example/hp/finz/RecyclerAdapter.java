package com.example.hp.finz;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    ViewHolder vh;
    private ArrayList mDataset;

    RecyclerAdapter mAdapter;
    public RecyclerAdapter(){


    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        // each data item is just a string in this case
        private String mItem;
        public TextView mTextView;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mTextView = (TextView) view;
        }
        public void setItem(String item) {
            mItem = item;
            mTextView.setText(item);
        }
        @Override
        public void onClick(View view) {

            Log.d("clicked", "onClick " + getPosition() + " " + mItem);
            Context context = view.getContext();
            Intent intent=new Intent(context,SelectedExpense.class);
            intent.putExtra("clicked",mItem);
            context.startActivity(intent);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapter(ArrayList myDataset) {
        mDataset = myDataset;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_text, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.setItem(mDataset.get(position).toString());


    }

//get number of items in arrayString
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
