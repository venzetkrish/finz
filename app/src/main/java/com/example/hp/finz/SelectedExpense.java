package com.example.hp.finz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SelectedExpense extends AppCompatActivity {
TextView recvText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_expense);
        recvText=(TextView)findViewById(R.id.selectText);
        //get intent value from recycler view and display
        String passedArg = getIntent().getStringExtra("clicked");
        recvText.setText(passedArg);
    }
}
