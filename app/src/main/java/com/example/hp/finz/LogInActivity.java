package com.example.hp.finz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogInActivity extends AppCompatActivity {
EditText emailEntry,passEntry;
Button signIn;
    SharedPreferences sp;
String emailId,pass,sEmail,sPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        emailEntry=(EditText)findViewById(R.id.emailId);
        passEntry=(EditText)findViewById(R.id.passWord);
        signIn=(Button)findViewById(R.id.signIn);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId = emailEntry.getText().toString();
                pass = passEntry.getText().toString();
                sp = getSharedPreferences("LoginValues", Context.MODE_PRIVATE);
                sEmail= sp.getString("email", "");
                sPass=sp.getString("pwd","");
                //Simple condition checking for login
                if (emailId.equals("") || pass.equals("")) {
                    Toast.makeText(getApplicationContext(),"Required Fields Empty",Toast.LENGTH_SHORT).show();
                }
                else{
                    if(sEmail.equals(emailId) && sPass.equals(pass)){
                        Toast.makeText(getApplicationContext(),"Login Successful",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LogInActivity.this,OnBoardActivity.class));
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Not Registered",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
 }
}