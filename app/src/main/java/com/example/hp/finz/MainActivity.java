package com.example.hp.finz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button signUp,signIn;
int logIn=0;
SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

            logIn = getIntent().getIntExtra("hello",0);

        if(logIn==0)
        {
        sharedPreferences=getSharedPreferences("LoginValues", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("email", "")!=""){

            startActivity(new Intent(this,FinzActivity.class));

        }}
        signIn=(Button)findViewById(R.id.signIn);
        signUp=(Button)findViewById(R.id.signUp);
        //on click GOTO loginActivity
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,LogInActivity.class));
            }
        });
        //on click GOTO signupActivity
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,SignUpActivity.class));
            }
        });
    }
}
