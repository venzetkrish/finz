package com.example.hp.finz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
EditText nameEntry,emailEntry,passwordEntry;
Button signUp;
String name,emailId,pass,emailPattern;
TextView signIn;
SharedPreferences sp,gp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if(!isFirstTimeStartApp())
        {
            startMainActivity();
            finish();
        }
        nameEntry=(EditText)findViewById(R.id.name);
        emailEntry=(EditText)findViewById(R.id.emailId);
        passwordEntry=(EditText)findViewById(R.id.password);
        signIn=(TextView)findViewById(R.id.signIn);
        signUp=(Button)findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=nameEntry.getText().toString();
                emailId=emailEntry.getText().toString();
                pass=passwordEntry.getText().toString();
                //pattern of email Id
                emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                //simple condition checking for signUP
                if(emailId.equals("")|| pass.equals("")|| name.equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Required Fields Empty",Toast.LENGTH_SHORT).show();
                }
                else{
                    if(emailId.matches(emailPattern)){
                        gp=getSharedPreferences("LoginValues", Context.MODE_PRIVATE);
                        SharedPreferences.Editor gd=gp.edit();
                        gd.putString("name", name );
                        gd.putString("email", emailId );
                        gd.putString("pwd", pass );
                        gd.commit();
                        //clearFields
                        nameEntry.setText("");
                        emailEntry.setText("");
                        passwordEntry.setText("");
                        Toast.makeText(getApplicationContext(),"SignUp Successful",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Wrong Email Id",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    signIn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(SignUpActivity.this,LogInActivity.class));
        }
    });
    }

    private boolean isFirstTimeStartApp(){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        return ref.getBoolean("FirstTimeStartFlag",true);
    }

    private void setFirstTimeStartStatus(boolean stt){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=ref.edit();
        editor.putBoolean("FirstTimeStartFlag",stt);
        editor.commit();
    }

    private void startMainActivity(){
        setFirstTimeStartStatus(false);
        startActivity(new Intent(SignUpActivity.this,LogInActivity.class));
        finish();
    }

}
